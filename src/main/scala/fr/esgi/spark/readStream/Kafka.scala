package fr.esgi.spark.readStream

import fr.esgi.spark.etl.Http.httpRequest
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{DataFrame, SparkSession}

class Kafka(host: String, port: String, topic: String) {
  val spark: SparkSession = SparkSession.builder().getOrCreate()
  val Server: String = s"$host:$port"
  val Topic: String = topic
  val hdfs_path: String = s"hdfs://namenode:9000/data/spark/$Topic"
  val df: DataFrame = readProducer()

  var http_query: StreamingQuery = _
  var hdfs_query: StreamingQuery = _

  def readProducer(): DataFrame = {
    spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", Server)
      .option("subscribe", Topic)
      .load()
      .selectExpr("CAST(value AS STRING)")
  }

  def writeStream(mode: String): Unit = {
    hdfs_query = getHdfsQuery(mode)
    if (Topic == "violation") {
      http_query = getHttpQuery(mode)
    }
  }

  def getHdfsQuery(mode: String): StreamingQuery = {
    df.writeStream
      .outputMode(mode)
      .format("csv")
      .option("checkpointLocation", s"$hdfs_path/checkpoint/")
      .option("path", s"$hdfs_path/data/")
      .start()
  }

  def getHttpQuery(mode: String): StreamingQuery = {
    df.writeStream
      .outputMode(mode)
      .foreachBatch { (batchDF: DataFrame, batchId: Long) =>
        httpRequest(batchDF)
      }
      .start()
  }
}
