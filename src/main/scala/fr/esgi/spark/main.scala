package fr.esgi.spark

import fr.esgi.spark.etl.Transform
import fr.esgi.spark.readStream.Kafka
import org.apache.spark.sql.SparkSession

object main {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().getOrCreate()
    val host = sys.env("KAFKA_HOST")
    val port = sys.env("KAFKA_PORT")

    List("position", "image", "violation").foreach(topic => {
      val kafka_consumer = new Kafka(host, port, topic)
      val df = Transform.transform_data(kafka_consumer)
      kafka_consumer.writeStream("append")
    })

    spark.streams.awaitAnyTermination()
  }
}
