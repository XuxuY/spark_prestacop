package fr.esgi.spark.etl

import org.apache.http.HttpHeaders
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.spark.sql.{DataFrame, Row}

import scala.util.parsing.json.JSONObject

object Http {
  def httpRequest(df: DataFrame): Unit = {
    df.collect().foreach(
      r => sendJson(getJson(r))
    )
  }

  def getJson(row: Row): String = {
    val m = row.getValuesMap(row.schema.fieldNames);
    return JSONObject(m).toString();
  }

  def sendJson(jsonStr: String): Unit = {
    val client = HttpClientBuilder.create().build()
    val post = new HttpPost("http://webapp_server:5000/ingest")
    post.addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
    post.setEntity(new StringEntity(jsonStr))
    val response = client.execute(post)
  }
}
