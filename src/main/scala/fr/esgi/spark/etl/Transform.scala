package fr.esgi.spark.etl

import fr.esgi.spark.readStream.Kafka
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, split}

object Transform {
  def transform_data(s: Kafka): DataFrame = {
    val df = s.df
    val topic_name = s.Topic

    val df_res =
      if (topic_name == "image")
        df.select(
          split(col("value"), ",").getItem(0).as("timestamp"),
          split(col("value"), ",").getItem(1).as("image_id"),
          split(col("value"), ",").getItem(2).as("image_b64")
        )
      else if (topic_name == "violation")
        df.select(
          split(col("value"), ",").getItem(0).as("timestamp"),
          split(col("value"), ",").getItem(1).as("drone_id"),
          split(col("value"), ",").getItem(2).as("altitude"),
          split(col("value"), ",").getItem(3).as("latitude"),
          split(col("value"), ",").getItem(4).as("longitude"),
          split(col("value"), ",").getItem(5).as("image_id"),
          split(col("value"), ",").getItem(6).as("violation_code")
        )
      else if (topic_name == "position")
        df.select(
          split(col("value"), ",").getItem(0).as("timestamp"),
          split(col("value"), ",").getItem(1).as("drone_id"),
          split(col("value"), ",").getItem(2).as("altitude"),
          split(col("value"), ",").getItem(3).as("latitude"),
          split(col("value"), ",").getItem(4).as("longitude")
        )
      else
        df
    df_res.drop("value")
  }
}
