package fr.esgi.spark.utils

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object SparkUtils {

  val sparkConf = new SparkConf()

  def spark(exercise: String, thread: String = "1"): SparkSession = {
    sparkConf.set("spark.app.name", exercise)
    sparkConf.set("spark.master", s"local[$thread]")

    SparkSession.builder()
      .config(sparkConf)
      .getOrCreate()
  }

  def openCsv(spark: SparkSession, path: String, sep: String = ",", head: Boolean = true): DataFrame = {
    spark.read
      .option("header", head)
      .option("separator", sep)
      .csv(path)
  }
}
