package fr.esgi.spark

import java.nio.file.Path

import fr.esgi.spark.main.{spark, sparkConf}
import org.apache.spark.SparkConf
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

object test {

  val sparkConf = new SparkConf()
  sparkConf.set("spark.app.name", "data-load")
  sparkConf.set("spark.master", "local[*]")
  val spark: SparkSession = SparkSession.builder()
    .config(sparkConf)
    .getOrCreate()

  def main(args: Array[String]): Unit = {
    val list = List("data-load/datas/data_2013-2014.csv", "data-load/datas/data_2015.csv", "data-load/datas/data_2016.csv", "data-load/datas/data_2017.csv")
    val df = openCsv(list).withColumn("year", get_year(col("Issue Date")))

    val violation_date: DataFrame = df
        .groupBy("year")
        .count()
        .orderBy(-col("count"))

    val month_year: DataFrame = df
      .withColumn("Issue Date", get_month_year(col("Issue Date")))
      .where(col("year").isin("2015", "2016", "2017"))
      .groupBy("Issue Date")
      .count()
      .orderBy(-col("count"))

    val state = df.where(col("year").isin("2015", "2016", "2017"))
      .groupBy("Registration State")
      .count()
      .orderBy(-col("count"))

    val code = df.where(col("year").isin("2015", "2016", "2017"))
      .where(col("Registration State") === "NY")
      .groupBy("Violation Code")
      .count()
      .orderBy(-col("count"))

    val v_make = df.where(col("year").isin("2015", "2016", "2017"))
      .where(col("Registration State") === "NY")
      .groupBy("Vehicle Make")
      .count()
      .orderBy(-col("count"))

//    code.show(false)
//    state.show(false)


    write(violation_date, "most_violation_year")
    write(month_year, "most_violation month")
    write(state, "viola_state")
    write(code, "violation_code")
    write(v_make, "marque_voit")
  }

  val get_year: UserDefinedFunction = udf((value: String) => {
    val y = value.split("/")(2)
    y
  })

  val get_month_year: UserDefinedFunction = udf((value: String) => {
    val m = value.split("/")(0)
    val y = value.split("/")(2)
    m.concat(s"/$y")
  })

  def openCsv(path: List[String], sep: String = ",", head: Boolean = true): DataFrame = {
    spark.read
      .option("header", head)
      .option("separator", sep)
      .csv(path:_*)
  }

  def write(df: DataFrame, path: String): Unit = {
    df.repartition(1).write.mode("overwrite").csv(s"stats/$path")
  }
}
