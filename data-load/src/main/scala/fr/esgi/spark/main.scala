package fr.esgi.spark

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}

object main {
  val hadoopUri = "hdfs://namenode:9000"
  val sparkConf: SparkConf = new SparkConf()
  val sc = new SparkContext(sparkConf)
  val hadoopConf: Configuration = sc.hadoopConfiguration
  hadoopConf.set("fs.defaultFS", hadoopUri)
  val spark: SparkSession = SparkSession.builder()
    .config(sparkConf)
    .getOrCreate()

  def main(args: Array[String]): Unit = {
    val csvFiles = FileSystem
      .get(hadoopConf)
      .listStatus(new Path("/data/raw"))
      .map(_.getPath.toString)
      .toList

    Console.println("INFO : Start loading ...")
    val df = openCsv(csvFiles)

    var renamed_df = df.withColumn("dt_part", transf_partition(col("Issue Date")))

    df.columns.foreach(col => {
      renamed_df = renamed_df.withColumnRenamed(col, col.trim.replaceAll("\\s", "_"))
    })

    renamed_df.repartition(1)
      .write.partitionBy("dt_part")
      .mode("overwrite")
      .format("parquet")
      .save(s"$hadoopUri/data/refine/data-history")

    Console.println("INFO : Data history successfully loaded !")
  }

  def openCsv(path: List[String], sep: String = ",", head: Boolean = true): DataFrame = {
    spark.read
      .option("header", head)
      .option("separator", sep)
      .csv(path: _*)
  }

  val transf_partition: UserDefinedFunction = udf((value: String) => {
    val m = value.split("/")(0)
    val y = value.split("/")(2)
    m.concat(y)
  })
}
