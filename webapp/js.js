$(document).ready(function () {
    window.setInterval(function () {
        $.ajax({
            url: "list",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                const l = $("#list");
                l.empty();
                l.append(`
                <tr id="headers">
                    <th>Datetime</th>
                    <th>Drone ID</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Altitude</th>
                    <th>Violation code</th>
                    <th>Image ID</th>
                </tr>
                `)
                for (const drone of res[0].drones) {
                    // const timestamp = parseFloat(drone.timestamp);
                    const date = new Date(parseFloat(drone.timestamp) * 1000);
                    const formattedTime = (
                        date.getFullYear() + '-' +
                        date.getMonth().toString().padStart(2, "0") + '-' +
                        date.getDay().toString().padStart(2, "0") + ' ' +
                        date.getHours().toString().padStart(2, "0") + ':' +
                        date.getMinutes().toString().padStart(2, "0") + ':' +
                        date.getSeconds().toString().padStart(2, "0") + '.' +
                        date.getMilliseconds()
                    );
                    const latitude = parseFloat(drone.latitude).toFixed(2).toString().padStart(7, "\xa0");
                    const longitude = parseFloat(drone.longitude).toFixed(2).toString().padStart(7, "\xa0");
                    const altitude = parseFloat(drone.altitude).toFixed(2).toString().padStart(7, "\xa0");
                    l.append(`
                    <tr data-id="${drone.id}">
                        <td>${formattedTime}</td>
                        <td>${drone.drone_id}</td>
                        <td>${latitude}</td>
                        <td>${longitude}</td>
                        <td>${altitude}</td>
                        <td>${drone.violation_code}</td>
                        <td>${drone.image_id}</td>
                        <td><button>Delete</button></td>
                    </tr>
                    `)
                }
            },
            error: function () {
                alert("Error : Cannot reach the server.")
            }
        });
    }, 1000);
});

$("table").on('click', 'button', function(e) {
    if (confirm("Are you really sure ?")) {
        const droneId = $(this).closest("tr").data("id");
        $.ajax({
            url: "delete",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify( { droneId: droneId }),
            dataType: 'json',
        });
    }
});
