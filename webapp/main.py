import time
from decimal import Decimal

import psycopg2
from flask import Flask, jsonify, make_response, request, send_file

app = Flask("webapp")


class Database:
    def __init__(self):
        self.host = "webapp_db"
        self.user = "postgres"
        self.password = "pWbqw5LxoSpLHx9dY5yq5CK2rPWFRxjwNJdGdToK"
        self.db = self.user
        self.psycopg2_params = {
            "host": self.host,
            "user": self.user,
            "password": self.password,
            "dbname": self.db,
        }

    def init_db(self):
        with psycopg2.connect(**self.psycopg2_params) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "CREATE TABLE IF NOT EXISTS drone ("
                    "id serial PRIMARY KEY, "
                    "timestamp numeric, "
                    "drone_id varchar, "
                    "latitude numeric, "
                    "longitude numeric, "
                    "altitude numeric, "
                    "violation_code integer, "
                    "image_id varchar "
                    ");"
                )
                conn.commit()

    def list_json(self):
        with psycopg2.connect(**self.psycopg2_params) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "SELECT id, timestamp, drone_id, latitude, longitude, altitude, violation_code, image_id FROM drone"
                )
                return [
                    {
                        "id": x[0],
                        "timestamp": str(x[1]),
                        "drone_id": x[2],
                        "latitude": str(x[3]),
                        "longitude": str(x[4]),
                        "altitude": str(x[5]),
                        "violation_code": x[6],
                        "image_id": x[7],
                    }
                    for x in cur.fetchall()
                ]

    def insert(self, j):
        with psycopg2.connect(**self.psycopg2_params) as conn:
            with conn.cursor() as cur:
                (
                    timestamp,
                    drone_id,
                    altitude,
                    latitude,
                    longitude,
                    image_id,
                    violation_code,
                ) = (j["value"].strip().split(","))
                cur.execute(
                    "INSERT INTO drone (timestamp, drone_id, latitude, longitude, altitude, violation_code, image_id) "
                    "VALUES (%s, %s, %s, %s, %s, %s, %s);",
                    (
                        Decimal(timestamp),
                        drone_id,
                        Decimal(latitude),
                        Decimal(longitude),
                        Decimal(altitude),
                        int(violation_code),
                        image_id,
                    ),
                )
                conn.commit()

    def delete(self, drone_id):
        with psycopg2.connect(**self.psycopg2_params) as conn:
            with conn.cursor() as cur:
                cur.execute("DELETE FROM drone WHERE id = %s;", (drone_id,))
                conn.commit()


@app.route("/", methods=["GET"])
def index():
    return send_file("index.html")


@app.route("/js.js")
def send_js():
    return send_file("js.js")


@app.route("/delete", methods=["POST"])
def delete():
    drone_id = request.get_json()["droneId"]
    Database().delete(drone_id)
    return make_response("SUCCESS", 404)


@app.route("/ingest", methods=["POST"])
def ingest():
    j = request.get_json()
    Database().insert(j)
    return make_response("SUCCESS", 201)


@app.route("/list", methods=["GET"])
def list():
    drone_list = Database().list_json()
    return jsonify({"drones": drone_list}, 200)


if __name__ == "__main__":
    while True:
        try:
            Database().init_db()
            break
        except psycopg2.OperationalError:
            time.sleep(1)

    app.run(host="0.0.0.0", port=5000, debug=True)
