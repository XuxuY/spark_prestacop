import random
import uuid
from time import sleep
from typing import List

from drone import Drone
from kafka import KafkaProducer
from kafka.errors import KafkaTimeoutError


def send_drone_data(producer: KafkaProducer, drones: List[Drone], event_p: float, sleep_time: int) -> None:
    while True:
        for drone in drones:
            try:
                producer.send("position", value=drone.csv())
                if random.random() < event_p:
                    image_id = str(uuid.uuid4())
                    producer.send("violation", value=drone.csv_violation(image_id))
                    producer.send("image", value=drone.csv_image(image_id))
            except (BrokenPipeError, ConnectionResetError, KafkaTimeoutError):
                return
        sleep(sleep_time)
