from drone import Drone
from kafka import KafkaProducer
from kafka.errors import NoBrokersAvailable
from utils import send_drone_data
from time import sleep
import os

HOST = os.environ["KAFKA_HOST"]  # The server's hostname or IP address
PORT = os.environ["KAFKA_PORT"]  # The port used by the server
EVENT_P = 0.05
KAFKA_SLEEP_TIME = 5
SLEEP_TIME = 3
DRONE_NUMBER = 10
DRONES = [Drone() for _ in range(DRONE_NUMBER)]

if __name__ == "__main__":
    kafka_server = f"{HOST}:{PORT}"
    producer = None
    while not producer:
        try:
            print(f"Trying to connect to Kafka : {kafka_server}")
            producer = KafkaProducer(bootstrap_servers=[kafka_server])
            print(f"Connected to Kafka : {kafka_server}")
        except NoBrokersAvailable:
            print(f"No brokers available at {kafka_server}, retrying in {KAFKA_SLEEP_TIME}s")
            sleep(KAFKA_SLEEP_TIME)

    while True:
        print("Connection start.")
        send_drone_data(producer, DRONES, EVENT_P, SLEEP_TIME)
        print("Connection has ended.")
