docker_clear:
	clear
	docker-compose down
	docker volume rm spark_prestacop_prestacop_hadoop_datanode spark_prestacop_prestacop_hadoop_namenode spark_prestacop_prestacop_jar || true

docker_run_from_scratch: docker_clear
	docker-compose up --build

load-data:
	docker exec -it datanode mkdir -p /app/lib
	docker exec -it datanode mkdir -p /data/raw
	docker cp data-load/target/spark-data-load-jar-with-dependencies.jar datanode:/app/lib/data-load.jar
	docker cp data-load/datas/. datanode:/data/raw/
	docker exec -it datanode hdfs dfs -mkdir -p /app/lib
	docker exec -it datanode hdfs dfs -mkdir -p /data/refine
	docker exec -it datanode hdfs dfs -put -f /app/lib/data-load.jar /app/lib
	docker exec -it datanode hdfs dfs -put -f /data/raw /data/
	docker exec prestacop_spark-master ./spark/bin/spark-submit --master spark://spark-master:7077 --deploy-mode cluster --conf spark.app.name=DATA-LOAD --class fr.esgi.spark.main hdfs://namenode:9000/app/lib/data-load.jar

submit:
	docker cp target/spark-prestacop-jar-with-dependencies.jar prestacop_spark-master:/prestacop_jar/spark-prestacop-jar-with-dependencies.jar
	docker exec prestacop_spark-master ./spark/bin/spark-submit --master spark://spark-master:7077 --deploy-mode cluster --conf spark.app.name=DRONE_DATA_STREAM --class fr.esgi.spark.main /prestacop_jar/spark-prestacop-jar-with-dependencies.jar
